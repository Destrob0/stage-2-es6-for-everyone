import { createElement } from '../helpers/domHelper';
import { clearFighter } from '../components/fighterSelector';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    // Create card container
    const cardElement = createElement({
      tagName: 'div',
      className: 'fighter-preview__card',
    });

    // Insert image
    const img = createFighterImage(fighter);
    const imgElement = createElement({
      tagName: 'div',
      className: `fighter-preview__hero`,
    });
    img.style.transform = `ScaleX(${position === 'right' ? '-1' : '1'})`;
    imgElement.append(img);

    // Insert name,health,atk,def
    const nameElement = createElement({
      tagName: 'span',
      className: 'fighter-preview__name',
    });
    nameElement.textContent = fighter.name;

    const healthElement = createElement({
      tagName: 'span',
      className: 'fighter-preview__health',
    });
    healthElement.textContent = 'Health: ' + fighter.health;

    const attackElement = createElement({
      tagName: 'span',
      className: 'fighter-preview__attack',
    });
    attackElement.textContent = 'Attack: ' + fighter.attack;

    const defenseElement = createElement({
      tagName: 'span',
      className: 'fighter-preview__defense',
    });
    defenseElement.textContent = 'Defense: ' + fighter.defense;

    const undoElement = createElement({
      tagName: 'div',
      className: 'fighter-preview__undo',
    });
    undoElement.dataset.position = position;
    undoElement.onclick = clearFighter;

    // Choose position for elements
    cardElement.append(nameElement, imgElement, healthElement, attackElement, defenseElement, undoElement);

    fighterElement.append(cardElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
