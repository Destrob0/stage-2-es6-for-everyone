import { showModal } from './modal'

export function showWinnerModal(fighter) {
  // call showModal function 
  showModal({ title: `${fighter.name} has won!`, bodyElement: "This game was perfect!", onClose: () => document.location.reload(true) });
}
