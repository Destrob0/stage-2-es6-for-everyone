import { controls } from '../../constants/controls';

const normalColor = '#ebd759';
const ultiColor = 'green';
const lowColor = 'red';

export async function fight(firstFighter, secondFighter) {

  return new Promise((resolve) => {

    const firstHero = Object.assign({}, firstFighter);
    const secondHero = Object.assign({}, secondFighter);

    const leftHP = document.getElementById('left-fighter-indicator').clientWidth / firstHero.health;
    const rightHP = document.getElementById('right-fighter-indicator').clientWidth / secondHero.health;

    let firstCooldown = true;
    let secondCooldown = true;

    document.getElementById('left-fighter-indicator').style.backgroundColor = ultiColor;
    document.getElementById('right-fighter-indicator').style.backgroundColor = ultiColor;

    let combinations = new Set();

    // Checks when you clicked special keys
    document.body.onkeydown = (event) => {
      combinations.add(event.code);

      if (controls.PlayerOneCriticalHitCombination.every(elem => combinations.has(elem)) && firstCooldown) {

        document.getElementById('left-fighter-indicator').style.backgroundColor = normalColor;

        let damage = 2 * firstHero.attack;
        secondHero.health -= damage;

        let damageHP = getHealth(damage, rightHP);
        checkHealthBar(damageHP, 'right');

        if (secondHero.health <= 0) resolve(firstHero);

        firstCooldown = false;
        setTimeout(() => {
          firstCooldown = true;
          document.getElementById('left-fighter-indicator').style.backgroundColor = ultiColor;
        }, 10000);

      }
      else if (controls.PlayerTwoCriticalHitCombination.every(elem => combinations.has(elem)) && secondCooldown) {

        document.getElementById('right-fighter-indicator').style.backgroundColor = normalColor;

        let damage = 2 * secondHero.attack;
        firstHero.health -= damage;

        let damageHP = getHealth(damage, leftHP);
        checkHealthBar(damageHP, 'left');

        if (firstHero.health <= 0) resolve(secondHero);

        secondCooldown = false;
        setTimeout(() => {
          secondCooldown = true;
          document.getElementById('right-fighter-indicator').style.backgroundColor = ultiColor;
        }, 10000);

      }
      else if (combinations.has(controls.PlayerOneAttack) && combinations.has(controls.PlayerTwoBlock)) {

        let damage = getDamage(firstHero, secondHero)

        if (damage < 0) damage = 0;
        else secondHero.health -= damage;


        let damageHP = getHealth(damage, rightHP);
        checkHealthBar(damageHP, 'right');

        if (secondHero.health <= 0) resolve(firstHero);
      }
      else if (combinations.has(controls.PlayerTwoAttack) && combinations.has(controls.PlayerOneBlock)) {

        let damage = getDamage(secondHero, firstHero)

        if (damage < 0) damage = 0;
        else firstHero.health -= damage;


        let damageHP = getHealth(damage, leftHP);
        checkHealthBar(damageHP, 'left');

        if (firstHero.health <= 0) resolve(secondHero);

      }
      else if (combinations.has(controls.PlayerOneAttack) && !combinations.has(controls.PlayerOneBlock)) {
        let damage = getHitPower(firstHero);
        secondHero.health -= damage;

        let damageHP = getHealth(damage, rightHP);
        checkHealthBar(damageHP, 'right');

        if (secondHero.health <= 0) resolve(firstHero);

      }
      else if (combinations.has(controls.PlayerTwoAttack) && !combinations.has(controls.PlayerTwoBlock)) {
        let damage = getHitPower(secondHero);
        firstHero.health -= damage;

        let damageHP = getHealth(damage, leftHP);
        checkHealthBar(damageHP, 'left');

        if (firstHero.health <= 0) resolve(secondHero);

      }

    };

    // Delete keys from combinations
    document.body.onkeyup = (event) => {
      combinations.delete(event.code);
    }


  });
}

export function getDamage(attacker, defender) {
  // return damage
  return getHitPower(attacker) - getBlockPower(defender);
}

export function getHitPower(fighter) {
  // return hit power
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  // return block power
  return fighter.defense * (Math.random() + 1);
}

export function getHealth(damage, HP) {
  //return changed hp
  return damage * HP;
}

export function checkHealthBar(damage, place) {
  if (place === 'right') {
    const rightHealthBar = document.getElementById('right-fighter-indicator');

    if ((rightHealthBar.clientWidth - damage) < 150) rightHealthBar.style.backgroundColor = lowColor;

    if ((rightHealthBar.clientWidth - damage) <= 0) rightHealthBar.style.width = 0;
    else rightHealthBar.style.width = (rightHealthBar.clientWidth - damage) + 'px';
  }
  else {
    const leftHealthBar = document.getElementById('left-fighter-indicator');

    if ((leftHealthBar.clientWidth - damage) < 150) leftHealthBar.style.backgroundColor = lowColor;

    if ((leftHealthBar.clientWidth - damage) <= 0) leftHealthBar.style.width = 0;
    else leftHealthBar.style.width = (leftHealthBar.clientWidth - damage) + 'px';
  }
}

